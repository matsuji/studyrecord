//
//  TextViewController.swift
//  StudyRecord
//
//  Created by 松本淳之介 on 2017/06/24.
//  Copyright © 2017年 Junnosuke Matsumto. All rights reserved.
//

import UIKit

class TextViewController: UIViewController {

    @IBOutlet fileprivate var tableView: UITableView!
    fileprivate let viewModel = TextViewModel()
    fileprivate weak var book :Text?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        configureView()
    }

    @IBAction func tapAddButton() {
        showEditAlert()
    }

    fileprivate func showEditAlert(text: Text? = nil) {
        let actionController = UIAlertController(title: "教科書の追加", message: "追加する教科書を入力してください", preferredStyle: .alert)
        actionController.addTextField { (textField) in
            textField.placeholder = "名前"
            textField.text = text?.name ?? ""
        }
        actionController.addTextField { (textField) in
            textField.placeholder = "ページ数"
            if let text = text {
                textField.text = "\(text.size)"
            }
            textField.keyboardType = UIKeyboardType.numberPad
        }
        let okAction = UIAlertAction(title: "追加", style: .default) { [weak self] (action) in
            guard let `self` = self,
                let name = actionController.textFields?[0].text,
                let pageString = actionController.textFields?[1].text else { return }
            guard let page = Int(pageString) else { return }

            self.viewModel.save(text: text,name: name, size: page)
        }
        let cancelAction = UIAlertAction(title: "キャンセル", style: .cancel, handler: nil)
        actionController.addAction(okAction)
        actionController.addAction(cancelAction)
        present(actionController, animated: true, completion: nil)
    }
}


// MARK: - View

extension TextViewController {

    fileprivate func configureView() {
        configureTableView()
        configureNavigationItem()

        viewModel.update = { [weak self] in
            self?.tableView.reloadData()
        }
    }

    private func configureTableView() {
        tableView.dataSource = self
        tableView.delegate = self
        tableView.registerCell(identifier: BookCell.identifier)
        tableView.estimatedRowHeight = 60.0
        tableView.rowHeight = UITableViewAutomaticDimension
    }

    private func configureNavigationItem() {
        navigationItem.title = "教科書"
    }
}


// MARK: - UITableViewDataSource

extension TextViewController: UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.countBooks
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: BookCell.identifier, for: indexPath) as! BookCell
        let text = viewModel.getBook(at: indexPath.row)
        cell.set(text: text)
        cell.imageHandler = { [weak self] book in
            guard let text = book else { return }
            self?.changeImage(text: text)
        }
        return cell
    }

    private func changeImage(text: Text) {
        let alertController = UIAlertController(title: "画像の追加", message: nil, preferredStyle: .alert)
        let cameraAction = UIAlertAction(title: "カメラ", style: .default, handler: { [weak self] _ in
            self?.showCamera()
        })
        let albumAction = UIAlertAction(title: "アルバム", style: .default, handler: { [weak self] _ in
            self?.showImagePicker()
        })
        let deleteAction = UIAlertAction(title: "削除", style: .destructive, handler: { [weak self] _ in
            self?.viewModel.setImage(text: text, image: nil)
        })
        let cancelAction = UIAlertAction(title: "キャンセル", style: .cancel, handler: nil)
        alertController.addAction(cameraAction)
        alertController.addAction(albumAction)
        alertController.addAction(deleteAction)
        alertController.addAction(cancelAction)
        self.book = text
        present(alertController, animated: true, completion: nil)
    }

    private func showImagePicker() {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) {
            let imagePickerController = UIImagePickerController()
            imagePickerController.sourceType = .photoLibrary
            imagePickerController.delegate = self
            present(imagePickerController, animated: true, completion: nil)
        }
    }

    private func showCamera() {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
            let picker = UIImagePickerController()
            picker.modalPresentationStyle = UIModalPresentationStyle.fullScreen
            picker.delegate = self // UINavigationControllerDelegate と　UIImagePickerControllerDelegateを実装する
            picker.sourceType = UIImagePickerControllerSourceType.camera
            present(picker, animated: true, completion: nil)
        }
    }
}


// MARK: - UITableViewDelegate

extension TextViewController: UITableViewDelegate {

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let book = viewModel.getBook(at: indexPath.row)
        showEditAlert(text: book)
    }

    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }

    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let action = UITableViewRowAction(style: .destructive, title: "消去") { [weak self](action, indexPath) in
            self?.tableView.beginUpdates()
            self?.viewModel.delete(at: indexPath.row)
            self?.tableView.deleteRows(at: [indexPath], with: .automatic)
            self?.tableView.endUpdates()
        }
        return [action]
    }
}


// MARK: -  UIImagePickerControllerDelegate & UINavigationControllerDelegate

extension TextViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let editedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            viewModel.setImage(text: book, image: editedImage)
        }
        picker.dismiss(animated: true, completion: nil)
    }
}
