//
//  SkipDayCell.swift
//  StudyRecord
//
//  Created by 松本淳之介 on 2017/07/07.
//  Copyright © 2017年 Junnosuke Matsumto. All rights reserved.
//

import UIKit

class SkipDayCell: UITableViewCell {

        
    @IBOutlet weak var skipTextField: UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
