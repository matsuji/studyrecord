//
//  ScheduleEditViewModel.swift
//  StudyRecord
//
//  Created by 松本淳之介 on 2017/06/24.
//  Copyright © 2017年 Junnosuke Matsumto. All rights reserved.
//

import Foundation
import RealmSwift

class ScheduleEditViewModel {

    private var schedule: Schedule?
    var text: Text?
    var startDay = Date()
    var endDay = Date()
    var page = 0
    private var skipDays: [SkipDay] = []
    var countSkipDays: Int {
        return skipDays.count
    }
    var isEnableSave: Bool {
        return text != nil
    }

    init(_ schedule: Schedule? = nil) {
        self.schedule = schedule
        if let schedule = schedule {
            text = schedule.text
            page = schedule.page
            startDay = schedule.startDay!
            endDay = schedule.endDay!
            skipDays = schedule.skipDays.map({ (s) -> SkipDay in
                let skipDay = SkipDay()
                skipDay.day = s.day
                return skipDay
            })
        }
    }

    func save(page: Int) {
        guard isEnableSave else { return }

        let realm = try! Realm()
        if schedule == nil {
            schedule = Schedule()
            schedule?.id = UUID().uuidString
        } else {
            try! realm.write {
                schedule!.skipDays.forEach({ (day) in
                    realm.delete(day)
                })
            }
        }
        try! realm.write {
            schedule?.text = text
            schedule?.page = page
            schedule?.startDay = startDay
            schedule?.endDay = endDay
            skipDays.forEach {
                schedule?.skipDays.append($0)
            }
            realm.add(schedule!, update: true)
        }
    }

    func addSkipDay(day: Date) {
        let skipDay = SkipDay()
        skipDay.day = day
        skipDays.append(skipDay)
    }

    func deleteDay(at index: Int) {
        skipDays.remove(at: index)
    }

    func editDay(at index: Int, day: Date) {
        let skipDay = SkipDay()
        skipDay.day = day
        skipDays[index] = skipDay
    }

    func skipDay(at index: Int) -> SkipDay {
        return skipDays[index]
    }
}
