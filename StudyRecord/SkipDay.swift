//
//  SkipDay.swift
//  StudyRecord
//
//  Created by 松本淳之介 on 2017/07/07.
//  Copyright © 2017年 Junnosuke Matsumto. All rights reserved.
//

import Foundation
import RealmSwift

class SkipDay: Object {
    dynamic var day: Date = Date()
}
