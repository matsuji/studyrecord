//
//  ChartCell.swift
//  StudyRecord
//
//  Created by 松本淳之介 on 2017/06/26.
//  Copyright © 2017年 Junnosuke Matsumto. All rights reserved.
//

import UIKit
import Charts

class ChartCell: UITableViewCell {
    
    @IBOutlet weak var combinedChartView: CombinedChartView!

    func setCharts() {
        let timeEntries = [
            BarChartDataEntry(x: 1, y: 5),
            BarChartDataEntry(x: 2, y: 4),
            BarChartDataEntry(x: 3, y: 5),
            BarChartDataEntry(x: 4, y: 4.5),
            BarChartDataEntry(x: 5, y: 1)
        ]
        let pageEntries = [
            BarChartDataEntry(x: 1, y: 30),
            BarChartDataEntry(x: 2, y: 23),
            BarChartDataEntry(x: 3, y: 28),
            BarChartDataEntry(x: 4, y: 25),
            BarChartDataEntry(x: 5, y: 10)
        ]
        let timeSet = BarChartDataSet(values: timeEntries, label: "勉強時間")
        let timeColor = UIColor.orange
        timeSet.colors = [timeColor]
        let pageSet = LineChartDataSet(values: pageEntries, label: "ページ数")
//        let pageColor = UIColor.purple
//        pageSet.colors = [pageColor]
//        pageSet.circleColors = [pageColor]

        let data = CombinedChartData()
        data.barData = BarChartData(dataSet: timeSet)
        data.lineData = LineChartData(dataSet: pageSet)
        combinedChartView.data = data
        combinedChartView.chartDescription?.text = ""
    }
}
