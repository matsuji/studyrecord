//
//  TextSelectViewController.swift
//  StudyRecord
//
//  Created by 松本淳之介 on 2017/06/24.
//  Copyright © 2017年 Junnosuke Matsumto. All rights reserved.
//

import UIKit
import RealmSwift

class TextSelectViewController: UITableViewController {

    fileprivate var books: Results<Text>?
    var select: (Text) -> Void = { _ in }

    override func viewDidLoad() {
        super.viewDidLoad()

        let realm = try! Realm()
        books = realm.objects(Text.self)
        tableView.registerCell(identifier: BookCell.identifier)
        tableView.estimatedRowHeight = 60.0
        tableView.rowHeight = UITableViewAutomaticDimension
    }
}

extension TextSelectViewController {

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return books?.count ?? 0
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: BookCell.identifier, for: indexPath) as! BookCell
        if let books = books {
            cell.set(text: books[indexPath.row])
        }
        return cell
    }
}

extension TextSelectViewController {

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let book = books![indexPath.row]
        select(book)
        navigationController?.popViewController(animated: true)
    }
}
