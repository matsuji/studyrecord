//
//  AppDelegate.swift
//  StudyRecord
//
//  Created by 松本淳之介 on 2017/06/16.
//  Copyright © 2017年 Junnosuke Matsumto. All rights reserved.
//

import UIKit
import RealmSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        migration()
        let vc = StudyTopViewController.createFromStoryboard()
        let naviVC = UINavigationController(rootViewController: vc)
        UINavigationBar.appearance().barTintColor = UIColor(red: 73 / 255, green: 207 / 255 , blue: 155 / 255, alpha: 1.0)
        UINavigationBar.appearance().tintColor = UIColor.white
        UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName : UIColor.white]
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.rootViewController = naviVC
        window?.makeKeyAndVisible()
        return true
    }

    private func migration() {
        let config = Realm.Configuration(
            schemaVersion: 8
        )
        Realm.Configuration.defaultConfiguration = config
        do {
            let _ = try Realm()
        } catch {
            fatalError()
        }
    }
}

