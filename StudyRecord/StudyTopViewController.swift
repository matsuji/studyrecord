//
//  ViewController.swift
//  StudyRecord
//
//  Created by 松本淳之介 on 2017/06/16.
//  Copyright © 2017年 Junnosuke Matsumto. All rights reserved.
//

import UIKit

class StudyTopViewController: UIViewController {

    @IBOutlet fileprivate weak var clockButton: UIButton!
    fileprivate let viewModel = StudyTopViewModel()

    override func viewDidLoad() {
        super.viewDidLoad()

        configureView()
    }

    @IBAction func tapClockButton() {
        let vc = RecorderViewController.createFromStoryboard()
        navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func tapRecordCell() {
        let vc = RecordListViewController.createFromStoryboard()
        navigationController?.pushViewController(vc, animated: true)
    }

    @IBAction func tapScheduleCell() {
        let vc = ScheduleViewController.createFromStoryboard()
        navigationController?.pushViewController(vc, animated: true)
    }

    @IBAction func tapTestCell() {
        let vc = TextViewController.createFromStoryboard()
        navigationController?.pushViewController(vc, animated: true)
    }

    @IBAction func tapSettingCell() {
        let vc = SettingViewController.createFromStoryboard()
        navigationController?.pushViewController(vc, animated: true)
    }
}


// MARK: - View

extension StudyTopViewController {

    fileprivate func configureView() {
        configureCloclButton()
        configureNavigationBar()
    }

    private func configureNavigationBar() {
        navigationItem.title = "スタレコ！"
    }

    private func configureCloclButton() {
        clockButton.layer.shadowColor = UIColor.black.cgColor
        clockButton.layer.shadowOffset = CGSize(width: 5.0, height: 5.0)
        clockButton.layer.shadowRadius = 5.0
        clockButton.layer.shadowOpacity = 0.3
    }
}
