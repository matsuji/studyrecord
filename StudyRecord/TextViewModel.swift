//
//  TextViewModel.swift
//  StudyRecord
//
//  Created by 松本淳之介 on 2017/06/24.
//  Copyright © 2017年 Junnosuke Matsumto. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift

class TextViewModel {

    private let books: Results<Text>
    private var notification: NotificationToken!
    var update: () -> Void = {}
    var countBooks: Int {
        return books.count
    }

    init() {
        let realm = try! Realm()
        books = realm.objects(Text.self)
        notification = realm.addNotificationBlock { [weak self] _,_ in
            self?.update()
        }
    }

    func getBook(at index: Int) -> Text {
        return books[index]
    }

    func save(text book: Text? = nil, name : String, size: Int) {
        if let text = book {
            Realm.write {
                text.name = name
                text.size = size
            }
        } else {
            let realm = try! Realm()
            let text = Text()
            text.id = UUID().uuidString
            text.name = name
            text.size = size
            try! realm.write {
                realm.add(text)
            }
        }
    }

    func setImage(text: Text?, image: UIImage?) {
        Realm.write {
            text?.image = image
        }
    }

    func delete(at index: Int) {
        let realm = try! Realm()
        let book = books[index]
        try! realm.write {
            book.schedule.forEach({ (schedule) in
                realm.delete(schedule)
            })
            realm.delete(book)
        }
    }
}
