//
//  ScheduleRecord.swift
//  StudyRecord
//
//  Created by 松本淳之介 on 2017/06/24.
//  Copyright © 2017年 Junnosuke Matsumto. All rights reserved.
//

import RealmSwift

class ScheduleRecord: Object {
    dynamic var schedule: Schedule?
    dynamic var page = 0
}
