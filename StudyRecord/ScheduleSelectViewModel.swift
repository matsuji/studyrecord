//
//  ScheduleSelectViewModel.swift
//  StudyRecord
//
//  Created by 松本淳之介 on 2017/06/25.
//  Copyright © 2017年 Junnosuke Matsumto. All rights reserved.
//

import Foundation
import RealmSwift

class ScheduleSelectViewModel {

    private var unselectedSchedules: [Schedule] = []
    private let record = Record()
    var countSelectedSchedules: Int {
        return record.contents.count
    }
    var countUnselectedSchedules: Int {
        return unselectedSchedules.count
    }

    init(_ time: Double) {
        record.id = UUID().uuidString
        record.time = time
        let realm = try! Realm()
        unselectedSchedules = Array(realm.objects(Schedule.self).map { $0 })
    }

    func getUnselectedSchedule(at index: Int) -> Schedule {
        return unselectedSchedules[index]
    }

    func getSelectedSchedule(at index: Int) -> ScheduleRecord {
        return record.contents[index]
    }

    func select(schedule: Schedule, page: Int) {
        let scheduleRecord = ScheduleRecord()
        scheduleRecord.schedule = schedule
        scheduleRecord.page = page
        record.contents.append(scheduleRecord)
        if let index = unselectedSchedules.index(where: { (s) -> Bool in
            return schedule.id == s.id
        }) {
            unselectedSchedules.remove(at: index)
        }
    }

    func unselect(schedule: Schedule) {
        if let index = record.contents.index(where: { $0.schedule!.id == schedule.id }) {
            record.contents.remove(objectAtIndex: index)
            unselectedSchedules.append(schedule)
        }
    }

    func save() {
        let realm = try! Realm()
        let date = Date()
        let components = date.components
        let dailyRecord = realm.objects(DailyRecord.self).filter("_year == %@ && _month == %@ && _day == %@", components.year!, components.month!, components.day!)
        if dailyRecord.isEmpty {
            let dailyRecord = DailyRecord()
            dailyRecord.date = date
            try! realm.write {
                realm.add(dailyRecord)
            }
        }
        try! realm.write {
            dailyRecord.first?.records.append(record)
        }
    }
}
