//
//  Realm+addition.swift
//  StudyRecord
//
//  Created by 松本淳之介 on 2017/06/24.
//  Copyright © 2017年 Junnosuke Matsumto. All rights reserved.
//

import RealmSwift

extension Realm {

    class func write(block: (() -> Void)) {
        try! (try! Realm()).write(block)
    }
}
