//
//  ScheduleSelectViewController.swift
//  StudyRecord
//
//  Created by 松本淳之介 on 2017/06/25.
//  Copyright © 2017年 Junnosuke Matsumto. All rights reserved.
//

import UIKit

class ScheduleSelectViewController: UIViewController {

    @IBOutlet fileprivate weak var tableView: UITableView!
    @IBOutlet weak var navigationBar: UINavigationBar!
    var previousViewController: UIViewController?

    fileprivate var viewModel: ScheduleSelectViewModel!

    override func viewDidLoad() {
        super.viewDidLoad()

        configureView()
    }

    static func createFromStoryboard(_ time: Double) -> ScheduleSelectViewController {
        let vc = ScheduleSelectViewController.createFromStoryboard()
        vc.viewModel = ScheduleSelectViewModel(time)
        return vc
    }
    @IBAction func tapSaveButton(_ sender: Any) {
        viewModel.save()
        let vc = previousViewController
        dismiss(animated: false) {
            RecorderManager.shared.startTime = nil
             vc?.navigationController?.popToRootViewController(animated: true)
        }
    }
}


// MARK: - View

extension ScheduleSelectViewController {

    fileprivate func configureView() {
        tableView.registerCell(identifier: ScheduleCell.identifier)

        tableView.delegate = self
        tableView.dataSource = self

        navigationBar.delegate = self
    }
}


// MARK: - UITableViewDataSource

extension ScheduleSelectViewController: UITableViewDataSource {

    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }

    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0 {
            return "選択済み"
        }
        return "未選択"
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return viewModel.countSelectedSchedules
        }
        return viewModel.countUnselectedSchedules
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ScheduleCell.identifier, for: indexPath) as! ScheduleCell
        let schedule = { () -> Schedule in
            if indexPath.section == 0 {
                let selectedSchedule = self.viewModel.getSelectedSchedule(at: indexPath.row)
                cell.donePage = selectedSchedule.page
                return selectedSchedule.schedule!
            } else {
                cell.donePage = nil
            }
            return self.viewModel.getUnselectedSchedule(at: indexPath.row)
        }()
        cell.set(schedule: schedule)
        return cell
    }
}


// MARK: - UITableViewDelegate

extension ScheduleSelectViewController: UITableViewDelegate {

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }

    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60.0
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            didUnselectSchedule(indexPath: indexPath)
        } else if indexPath.section == 1 {
            didSelectSchedule(indexPath: indexPath)
        }
    }

    private func didSelectSchedule(indexPath: IndexPath) {
        let schedule = viewModel.getUnselectedSchedule(at: indexPath.row)
        let alertController = UIAlertController(title: nil, message: "ページ数を入力してください", preferredStyle: .alert)
        alertController.addTextField { (textField) in
            textField.placeholder = "ページ数"
            textField.keyboardType = .numberPad
        }
        let okAction = UIAlertAction(title: "OK", style: .default) { [weak self] (action) in
            guard let p = alertController.textFields?.first?.text else { return }
            guard let page = Int(p) else { return }

            self?.viewModel.select(schedule: schedule, page: page)
            self?.tableView.reloadData()
        }
        alertController.addAction(okAction)
        let cancelAction = UIAlertAction(title: "キャンセル", style: .cancel, handler: nil)
        alertController.addAction(cancelAction)
        present(alertController, animated: true, completion: nil)
    }

    private func didUnselectSchedule(indexPath: IndexPath) {
        guard let schedule = viewModel.getSelectedSchedule(at: indexPath.row).schedule else { return }

        viewModel.unselect(schedule: schedule)
        tableView.reloadData()
    }
}


// MARK: -
extension ScheduleSelectViewController: UINavigationBarDelegate {

    func position(for bar: UIBarPositioning) -> UIBarPosition {
        return UIBarPosition.topAttached
    }
}
