//
//  ScheduleViewModel.swift
//  StudyRecord
//
//  Created by 松本淳之介 on 2017/06/24.
//  Copyright © 2017年 Junnosuke Matsumto. All rights reserved.
//

import Foundation
import RealmSwift

final class SchedulerViewModel {
    private let schedules: Results<Schedule>
    private var notification: NotificationToken!
    private var totalPageCache = Dictionary<String, Int>()
    var updateHandler: () -> Void = {}
    var countItems: Int {
        return schedules.count
    }

    init() {
        let realm = try! Realm()
        schedules = realm.objects(Schedule.self).sorted(byKeyPath: "endDay")
        notification = realm.addNotificationBlock { [weak self] _, _ in
            self?.updateHandler()
        }
    }

    func getItems(at index: Int) -> Schedule {
        return schedules[index]
    }
    
    func delete(schedule: Schedule) {
        let realm = try! Realm()
        realm.beginWrite()
        realm.delete(schedule)
        try! realm.commitWrite(withoutNotifying: [notification])
    }

    func getPage(at index: Int) -> Int {
        let schedule = getItems(at: index)
        if let page = totalPageCache[schedule.id] {
            return Int(page)
        }
        let page = schedule.records.reduce(0) { (result, scheduleRecord) -> Int in
            return result + scheduleRecord.page
        }
        return page
    }
}
