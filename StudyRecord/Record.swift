//
//  Record.swift
//  StudyRecord
//
//  Created by 松本淳之介 on 2017/06/23.
//  Copyright © 2017年 Junnosuke Matsumto. All rights reserved.
//

import RealmSwift

class Record: Object {
    dynamic var id = ""
    dynamic var time = 0.0
    let contents = List<ScheduleRecord>()

    override class func primaryKey() -> String? {
        return "id"
    }
}
