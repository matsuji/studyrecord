//
//  NSObject+addition.swift
//  StudyRecord
//
//  Created by 松本淳之介 on 2017/06/23.
//  Copyright © 2017年 Junnosuke Matsumto. All rights reserved.
//

import Foundation

extension NSObject {
    class var className: String {
        return String(describing: self)
    }

    var className: String {
        return type(of: self).className
    }
}
