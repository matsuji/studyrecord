//
//  RecorderViewModel.swift
//  StudyRecord
//
//  Created by 松本淳之介 on 2017/06/25.
//  Copyright © 2017年 Junnosuke Matsumto. All rights reserved.
//

import Foundation

class RecorderViewModel {

    private let manager = RecorderManager.shared
    private var timer: Timer!
    var updateHandler: () -> Void = {}
    var nowTime: TimeInterval {
        guard let date1 = manager.startTime else { return 0 }

        return -1 * date1.timeIntervalSinceNow
    }
    var startTime: Date? {
        return manager.startTime
    }

    init() {
        if manager.startTime != nil {
            fire()
        }
    }
    @objc private func update() {
        updateHandler()
    }

    private func fire() {
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.update), userInfo: nil, repeats: true)
        timer.fire()
    }

    func startRecord() {
        if manager.startTime == nil {
            manager.startTime = Date()
            fire()
        }
    }

    func finish() {
        timer.invalidate()
    }
}
