//
//  UITextField+addition.swift
//  StudyRecord
//
//  Created by 松本淳之介 on 2017/07/07.
//  Copyright © 2017年 Junnosuke Matsumto. All rights reserved.
//

import UIKit

extension UITextField {
    
    func setDoneButton() {
        let toolBar:UIToolbar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.sizeToFit()

        let spacer:UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
        let doneButton:UIBarButtonItem = UIBarButtonItem(title: "完了", style: .done, target: self, action: #selector(UITextField.closeKeyboard(textView:)))
        let toolBarItems = [spacer, doneButton]
        toolBar.setItems(toolBarItems, animated: true)
        self.inputAccessoryView = toolBar
    }

    @objc private func closeKeyboard(textView : UITextView){
        self.resignFirstResponder()
    }
}
