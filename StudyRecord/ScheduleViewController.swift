//
//  ScheduleViewController.swift
//  StudyRecord
//
//  Created by 松本淳之介 on 2017/06/24.
//  Copyright © 2017年 Junnosuke Matsumto. All rights reserved.
//

import UIKit

class ScheduleViewController: UIViewController {

    @IBOutlet fileprivate weak var tableView: UITableView!
    fileprivate let viewModel = SchedulerViewModel()

    override func viewDidLoad() {
        super.viewDidLoad()

        configureView()
    }

    @IBAction func tapAddButton() {
        let vc = ScheduleEditViewController.createFromStoryboard()
        navigationController?.pushViewController(vc, animated: true)
    }
}


// MARK: - View

extension ScheduleViewController {

    fileprivate func configureView() {
        configuretableView()

        navigationItem.title = "スケジュール一覧"
        viewModel.updateHandler = { [weak self] in
            self?.tableView.reloadData()
        }
    }

    private func configuretableView() {
        tableView.dataSource = self
        tableView.delegate = self
        tableView.estimatedRowHeight = 60.0
        tableView.rowHeight = UITableViewAutomaticDimension

        tableView.registerCell(identifier: ScheduleCell.identifier)
    }
}


// MARK: - UITableViewDataSource

extension ScheduleViewController: UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.countItems
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ScheduleCell.identifier, for: indexPath) as! ScheduleCell
        let schedule = viewModel.getItems(at: indexPath.row)
        cell.set(schedule: schedule)
        cell.donePage = viewModel.getPage(at: indexPath.row)
        return cell
    }
}


// MARK: - UITableViewDelegate

extension ScheduleViewController: UITableViewDelegate {

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let schedule = viewModel.getItems(at: indexPath.row)
        let vc = ScheduleEditViewController.createFromStoryboard(schedule)
        navigationController?.pushViewController(vc, animated: true)
    }

    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }

    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let action = UITableViewRowAction(style: .destructive, title: "消去", handler: { [weak self] action, indexPath in
            let schedule = self?.viewModel.getItems(at: indexPath.row)
            self?.tableView.beginUpdates()
            self?.viewModel.delete(schedule: schedule!)
            self?.tableView.deleteRows(at: [indexPath], with: UITableViewRowAnimation.automatic)
            self?.tableView.endUpdates()
        })
        return [action]
    }
}
