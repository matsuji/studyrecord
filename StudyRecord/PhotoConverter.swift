//
//  PhotoConverter.swift
//  StudyRecord
//
//  Created by 松本淳之介 on 2017/06/24.
//  Copyright © 2017年 Junnosuke Matsumto. All rights reserved.
//

import Foundation
import UIKit

final class PhotoConverter {

    static func convertDataFromImage(image: UIImage, size: Int) -> Data? {
        var qualityUse: CGFloat = 1.00
        while qualityUse >= 0.05 {
            if let data = UIImageJPEGRepresentation(image, qualityUse) {
                if data.count < size {
                    return data
                }
            }
            qualityUse -= 0.05
        }
        return nil
    }
}
