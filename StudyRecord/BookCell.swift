//
//  BookCell.swift
//  StudyRecord
//
//  Created by 松本淳之介 on 2017/06/24.
//  Copyright © 2017年 Junnosuke Matsumto. All rights reserved.
//

import UIKit

class BookCell: UITableViewCell {

    @IBOutlet weak var textImageView: UIImageView!
    @IBOutlet weak var textNameLabel: UILabel!
    @IBOutlet weak var pageLabel: UILabel!
    private var book: Text?
    var imageHandler: (Text?) -> Void = { _ in }

    override func awakeFromNib() {
        let gesture = UITapGestureRecognizer(target: self, action: #selector(self.tapImage(_:)))
        textImageView.addGestureRecognizer(gesture)

        super.awakeFromNib()
    }

    func set(text: Text) {
        self.book = text
        textNameLabel.text = text.name
        pageLabel.text = "\(text.size)P"
        if let image = text.image {
            textImageView.image = image
        } else {
            textImageView.image = UIImage(named: "noimage")
        }
    }

    @objc private func tapImage(_ sender: UITapGestureRecognizer) {
        imageHandler(book)
    }
}
