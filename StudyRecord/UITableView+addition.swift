//
//  UITableView+addition.swift
//  StudyRecord
//
//  Created by 松本淳之介 on 2017/06/23.
//  Copyright © 2017年 Junnosuke Matsumto. All rights reserved.
//

import UIKit

extension UITableView {

    func registerCell(identifier: String) {
        let nib = UINib(nibName: identifier, bundle: nil)
        register(nib, forCellReuseIdentifier: identifier)
    }
}


