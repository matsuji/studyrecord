//
//  Util.swift
//  StudyRecord
//
//  Created by 松本淳之介 on 2017/06/24.
//  Copyright © 2017年 Junnosuke Matsumto. All rights reserved.
//

import Foundation

func stringFromDate(_ date: Date, format: String) -> String {
    let formatter: DateFormatter = DateFormatter()
    formatter.dateFormat = format
    formatter.locale = Locale(identifier: "en_US_POSIX")
    return formatter.string(from: date)
}

func getIntervalDays(date:Date?,anotherDay:Date? = nil) -> Double {
    var retInterval:Double!
    if anotherDay == nil {
        retInterval = date?.timeIntervalSinceNow
    } else {
        retInterval = date?.timeIntervalSince(anotherDay!)
    }
    let ret = retInterval/86400
    return floor(ret)
}
