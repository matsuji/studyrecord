//
//  Date+addition.swift
//  StudyRecord
//
//  Created by 松本淳之介 on 2017/07/31.
//  Copyright © 2017年 Junnosuke Matsumto. All rights reserved.
//

import Foundation

extension Date {
    var components: DateComponents {
        let calender = Calendar.current
        let comp = calender.dateComponents([.year, .month, .day, .hour, .minute], from: self)
        return comp
    }
}
