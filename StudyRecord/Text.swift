//
//  Text.swift
//  StudyRecord
//
//  Created by 松本淳之介 on 2017/06/23.
//  Copyright © 2017年 Junnosuke Matsumto. All rights reserved.
//

import RealmSwift
import UIKit

class Text: Object {
    dynamic var id = ""
    dynamic var name = ""
    dynamic var size = 0
    @objc private var cache: UIImage?
    @objc private dynamic var _image: Data?
    var image: UIImage? {
        get {
            if let cache = cache {
                return cache
            }
            guard let data = _image else { return nil }

            return UIImage(data: data)
        }
        set {
            cache = newValue
            if let newValue = newValue {
                _image = PhotoConverter.convertDataFromImage(image: newValue, size: 3 * 1024 * 1024)
            } else {
                _image = nil
            }
        }
    }
    let schedule = LinkingObjects(fromType: Schedule.self, property: "text")

    override class func primaryKey() -> String? {
        return "id"
    }

    override class func ignoredProperties() -> [String] {
        return ["cache", "image"]
    }
}
