//
//  UIViewController+adition.swift
//  StudyRecord
//
//  Created by 松本淳之介 on 2017/06/16.
//  Copyright © 2017年 Junnosuke Matsumto. All rights reserved.
//

import UIKit

extension UIViewController {

    static func createFromStoryboard() -> Self {
        let name = String(describing: type(of: self)).components(separatedBy: ".").first!
        return instantiateFromStoryboard(storyboardName: name)
    }

    private static func instantiateFromStoryboard<T>(storyboardName: String) -> T {
        let storyboard = UIStoryboard(name: storyboardName, bundle: nil)
        return storyboard.instantiateInitialViewController() as! T
    }
}
