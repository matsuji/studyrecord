//
//  DailyRecordCell.swift
//  StudyRecord
//
//  Created by 松本淳之介 on 2017/06/23.
//  Copyright © 2017年 Junnosuke Matsumto. All rights reserved.
//

import UIKit

class DailyRecordCell: UITableViewCell {

    @IBOutlet private weak var dayLabel: UILabel!
    @IBOutlet private weak var timeLabel: UILabel!
    @IBOutlet private weak var pageLabel: UILabel!

    func set(_ dailyRecord: DailyRecord) {
        guard let _ = dailyRecord.date else {
            return
        }
        dayLabel.text = "\(dailyRecord.month!)月\(dailyRecord.day!)日"
        timeLabel.text = String(dailyRecord.records.reduce(0, { (result, record) -> Int in
            return result + Int(record.time)
        }))
        pageLabel.text = String(dailyRecord.records.reduce(0, { (result, record) -> Int in
            return result + record.contents.reduce(0, { (result, scheduleRecord) -> Int in
                return result + scheduleRecord.page
            })
        })) + "P"
    }
}
