//
//  RecordListViewModel.swift
//  StudyRecord
//
//  Created by 松本淳之介 on 2017/06/25.
//  Copyright © 2017年 Junnosuke Matsumto. All rights reserved.
//

import Foundation
import RealmSwift

class RecordListViewModel {

    private let records: Results<DailyRecord>
    var countItems: Int {
        return records.count
    }

    init() {
        let realm = try! Realm()
        records = realm.objects(DailyRecord.self)
    }

    func item(at index: Int) -> DailyRecord {
        return records[index]
    }
//
//    func delete(at index: Int) {
//        let realm = try! Realm()
//        try! realm.write {
//            let record = records[index]
//            realm.delete(record)
//        }
//    }
}
