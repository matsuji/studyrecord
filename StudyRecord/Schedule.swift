//
//  Schedule.swift
//  StudyRecord
//
//  Created by 松本淳之介 on 2017/06/24.
//  Copyright © 2017年 Junnosuke Matsumto. All rights reserved.
//

import RealmSwift

class Schedule: Object {
    dynamic var id = ""
    dynamic var text: Text?
    dynamic var page = 0
    dynamic var startDay: Date?
    dynamic var goalDay: Date?
    dynamic var endDay: Date?
    let records = LinkingObjects(fromType: ScheduleRecord.self, property: "schedule")
    let skipDays = List<SkipDay>()

    override class func primaryKey() -> String? {
        return "id"
    }
}
