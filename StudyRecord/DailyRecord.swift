//
//  DailyRecord.swift
//  StudyRecord
//
//  Created by 松本淳之介 on 2017/06/23.
//  Copyright © 2017年 Junnosuke Matsumto. All rights reserved.
//

import RealmSwift

class DailyRecord: Object {
    dynamic private var id = ""
    dynamic private var _date: Date?
    var date: Date? {
        get {
            return _date
        }
        set {
            _date = newValue
            let components = date?.components
            year = components?.year
            month = components?.month
            day = components?.day
            if let newValue = newValue {
                id = newValue.description
            }
        }
    }
    private(set) var year: Int? {
        get {
            return _year.value
        }
        set {
            _year.value = newValue
        }
    }
    private(set) var month: Int? {
        get {
            return _month.value
        }
        set {
            _month.value = newValue
        }
    }
    private(set) var day: Int? {
        get {
            return _day.value
        }
        set {
            _day.value = newValue
        }
    }
    private var _year = RealmOptional<Int>()
    private var _month = RealmOptional<Int>()
    private var _day = RealmOptional<Int>()
    let records = List<Record>()

    override class func ignoredProperties() -> [String] {
        return ["date", "year", "month", "day"]
    }

    override class func primaryKey() -> String? {
        return "id"
    }
}
