//
//  RecorderManager.swift
//  StudyRecord
//
//  Created by 松本淳之介 on 2017/06/25.
//  Copyright © 2017年 Junnosuke Matsumto. All rights reserved.
//

import Foundation

class RecorderManager {

    static let shared = RecorderManager()

    var startTime: Date?

    private init() {}

}
