//
//  ScheduleEditViewController.swift
//  StudyRecord
//
//  Created by 松本淳之介 on 2017/06/24.
//  Copyright © 2017年 Junnosuke Matsumto. All rights reserved.
//

import UIKit

class ScheduleEditViewController: UITableViewController {

    @IBOutlet fileprivate weak var startDayTextField: UITextField!
    @IBOutlet fileprivate weak var endDayTextField: UITextField!
    @IBOutlet fileprivate weak var pageTextField: UITextField!
    @IBOutlet fileprivate weak var bookLabel: UILabel!
    fileprivate var viewModel = ScheduleEditViewModel()
    fileprivate weak var textField: UITextField?

    override func viewDidLoad() {
        super.viewDidLoad()

        configureView()
    }

    static func createFromStoryboard(_ schedule: Schedule) -> ScheduleEditViewController {
        let vc = ScheduleEditViewController.createFromStoryboard()
        vc.viewModel = ScheduleEditViewModel(schedule)
        return vc
    }
}


// MARK: - View

extension ScheduleEditViewController {

    fileprivate func configureView() {
        if let text = viewModel.text {
            bookLabel.text = text.name
        }
        pageTextField.text = "\(viewModel.page)"
        tableView.keyboardDismissMode = .onDrag
        navigationItem.title = "スケジュールの編集"

        tableView.registerCell(identifier: SkipDayCell.identifier)
        startDayTextField.tag = 0
        endDayTextField.tag = 1
        [startDayTextField, endDayTextField].forEach {
            generateDatePicker(tag: $0!.tag, textField: $0!, date: Date())
        }
        startDayTextField.text = stringFromDate(viewModel.startDay, format: "MM月dd日")
        endDayTextField.text = stringFromDate(viewModel.endDay, format: "MM月dd日")
        let item = UIBarButtonItem(title: "保存", style: .done, target: self, action: #selector(self.save))
        navigationItem.rightBarButtonItem = item
    }

    @objc private func save() {
        guard let pageString = pageTextField.text else { return }
        let page = Int(pageString) ?? 0
        viewModel.save(page: page)
        navigationController?.popViewController(animated: true)
    }

    fileprivate func generateDatePicker(tag: Int, textField: UITextField, date: Date, isDatapicker: Bool = false) {
        let datePicker = UIDatePicker()
        datePicker.addTarget(self, action: #selector(self.datePickerViewDidChanged(sender:)), for: .valueChanged)
        datePicker.locale = Locale(identifier: "ja_JP")
        datePicker.tag = tag
        datePicker.date = date
        datePicker.datePickerMode = .date
        textField.delegate = self
        if isDatapicker {
            datePicker.datePickerMode = .date
        }
        textField.inputView = datePicker
    }

    @objc private func datePickerViewDidChanged(sender: UIDatePicker) {
        if sender.tag == startDayTextField.tag {
            startDayTextField.text = stringFromDate(sender.date, format: "MM月dd日")
            viewModel.startDay = sender.date
        } else if sender.tag == endDayTextField.tag {
            viewModel.endDay = sender.date
            endDayTextField.text = stringFromDate(sender.date, format: "MM月dd日")
        } else {
            let date = sender.date
            viewModel.editDay(at: sender.tag - endDayTextField.tag - 1, day: date)
            textField?.text = stringFromDate(date, format: "MM月dd日")
        }
    }
}


// MARK: - UItableViewDelegate

extension ScheduleEditViewController {

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            if indexPath.row == 2  {
                let vc = TextSelectViewController.createFromStoryboard()
                vc.select = { [weak self] book in
                    self?.viewModel.text = book
                    self?.bookLabel.text = book.name
                }
                navigationController?.pushViewController(vc, animated: true)
            }
        } else if indexPath.section == 1 {
            if indexPath.row == 0 {
                viewModel.addSkipDay(day: Date())
                tableView.reloadData()
            }
        }
    }
}


// MARK: - UITableViewDataSource

extension ScheduleEditViewController {

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return super.tableView(tableView, numberOfRowsInSection: section)
        }
        return viewModel.countSkipDays + 1
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 1 && indexPath.row != 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: SkipDayCell.identifier, for: indexPath) as! SkipDayCell
            let day = viewModel.skipDay(at: indexPath.row - 1).day
            cell.skipTextField.text = stringFromDate(day, format: "MM月dd日")
            generateDatePicker(tag: indexPath.row + endDayTextField.tag, textField: cell.skipTextField, date: day)
            return cell
        }
        return super.tableView(tableView, cellForRowAt: indexPath)
    }

    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return false
    }

    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        return false
    }

    override func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
        return UITableViewCellEditingStyle.none
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 1 && indexPath.row != 0 {
            return -1
        }
        return super.tableView(tableView, heightForRowAt: indexPath)
    }

    override func tableView(_ tableView: UITableView, indentationLevelForRowAt indexPath: IndexPath) -> Int {
        if indexPath.section == 1 {
            return Int(super.tableView(tableView, heightForRowAt: IndexPath(row: 0, section: indexPath.section)))
        } else {
            return Int(super.tableView(tableView, heightForRowAt: indexPath))
        }
    }
}


// MARK: UITextFieldDelegate

extension ScheduleEditViewController: UITextFieldDelegate {

    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.textField = textField
    }
}
