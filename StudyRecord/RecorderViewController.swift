//
//  CountTimerViewController.swift
//  StudyRecord
//
//  Created by 松本淳之介 on 2017/06/23.
//  Copyright © 2017年 Junnosuke Matsumto. All rights reserved.
//

import UIKit

class RecorderViewController: UIViewController {

    fileprivate let viewModel = RecorderViewModel()
    @IBOutlet fileprivate weak var timerLabel: UILabel!
    @IBOutlet fileprivate weak var startTimeLabel: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()

        configureView()
        viewModel.startRecord()
    }

    @IBAction func tapEndButton() {
        let alertController = UIAlertController(title: nil, message: "勉強を終了しますか？", preferredStyle: .alert)
        let action = UIAlertAction(title: "終了", style: .default) { [weak self] (aciont) in
            let time = self!.viewModel.nowTime
            let vc = ScheduleSelectViewController.createFromStoryboard(time)
            vc.previousViewController = self
            self?.present(vc, animated: true, completion: nil)
        }
        let cancel = UIAlertAction(title: "キャンセル", style: .default, handler: nil)
        alertController.addAction(cancel)
        alertController.addAction(action)
        present(alertController, animated: true, completion: nil)
    }

    @IBAction func tapBreakButton() {
    }
}


// MARK: - View

extension RecorderViewController {

    fileprivate func configureView() {
        viewModel.updateHandler = { [weak self] in
            self?.update()
        }
        update()
    }

    private func update() {
        let span = Int(viewModel.nowTime)
        let minute: Int = span / 60
        let sec: Int = span % 60
        let secString = sec < 10 ? "0" + String(sec) : String(sec)
        timerLabel.text = String(minute) + ":" + secString

        startTimeLabel.isHidden = viewModel.startTime == nil
        if let startTime = viewModel.startTime {
            let calender = Calendar(identifier: .gregorian)
            let hour = calender.component(.hour, from: startTime)
            let minute = calender.component(.minute, from: startTime)
            let minuteString = minute < 10 ? "0" + String(minute) : String(minute)
            startTimeLabel.text = String(hour) + ":" + minuteString + "に開始"
        }
    }
}
