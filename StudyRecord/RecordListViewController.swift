//
//  RecordListViewController.swift
//  StudyRecord
//
//  Created by 松本淳之介 on 2017/06/24.
//  Copyright © 2017年 Junnosuke Matsumto. All rights reserved.
//

import UIKit

class RecordListViewController: UIViewController {

    @IBOutlet fileprivate weak var tableView: UITableView!
    fileprivate let viewModel = RecordListViewModel()

    override func viewDidLoad() {
        super.viewDidLoad()

        configureView()
    }
}


//MARK: - UIView

extension RecordListViewController {

    fileprivate func configureView() {
        navigationItem.title = "学習記録一覧"
        tableView.dataSource = self
        tableView.delegate = self
        tableView.registerCell(identifier: ChartCell.identifier)
        tableView.registerCell(identifier: DailyRecordCell.identifier)
    }
}


// MARK: - UITableViewDataSource

extension RecordListViewController: UITableViewDataSource {

    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        }
        return viewModel.countItems
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: ChartCell.identifier, for: indexPath) as! ChartCell
            cell.setCharts()
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: DailyRecordCell.identifier, for: indexPath) as! DailyRecordCell
            cell.set(viewModel.item(at: indexPath.row))
            return cell
        }
    }
}


// MARK: - UITableViewDelegate

extension RecordListViewController: UITableViewDelegate {

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return view.frame.height / 3
        }
        return UITableViewAutomaticDimension
    }

    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60.0
    }

//    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
//        return indexPath.section == 1
//    }
//
//    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
//        let action = UITableViewRowAction(style: .destructive, title: "消去") { [weak self](action, indexPath) in
//            self?.tableView.beginUpdates()
//            self?.viewModel.delete(at: indexPath.row)
//            self?.tableView.deleteRows(at: [indexPath], with: .automatic)
//            self?.tableView.endUpdates()
//        }
//        return [action]
//    }
}
