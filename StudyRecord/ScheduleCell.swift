//
//  ScheduleCell.swift
//  StudyRecord
//
//  Created by 松本淳之介 on 2017/06/24.
//  Copyright © 2017年 Junnosuke Matsumto. All rights reserved.
//

import UIKit

class ScheduleCell: UITableViewCell {

    @IBOutlet private weak var nameLabel: UILabel!
    @IBOutlet private weak var pageLabel: UILabel!
    @IBOutlet private weak var restDayLabel: UILabel!
    @IBOutlet private weak var donePageLaebl: UILabel!
    @IBOutlet private weak var divLabel: UILabel!
    var donePage: Int? {
        set {
            let hidden = newValue == nil
            donePageLaebl.isHidden = hidden
            divLabel.isHidden = hidden
            guard let newValue = newValue else { return }

            donePageLaebl.text = String(newValue)
        }
        get {
            return Int(donePageLaebl.text!)
        }
    }

    func set(schedule: Schedule) {
        if let textName = schedule.text?.name {
            nameLabel.text = textName
        }
        pageLabel.text = String(schedule.page) + "P"
        restDayLabel.text = String(calculatreRestday(schedule))
    }

    private func calculatreRestday(_ schedule: Schedule) -> Int {
        guard let date = schedule.endDay else { return 12 }
        let anotherDate = Date()
        return Int(getIntervalDays(date: date, anotherDay: anotherDate))
    }
}
